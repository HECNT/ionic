var url = 'http://localhost:4001'
angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http, $ionicPopup) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  var sesion = localStorage.getItem("sesion");
  if (sesion == '1') {
    $scope.show_sesion = true
  } else {
    $scope.show_sesion = false
    location.href = '#/app/inicio'
  }
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    var d = $scope.loginData
    $http.post('https://api-quinfort.herokuapp.com/ses', d)
    .then(function(res){
      if (res.data.err) {
        // $scope.show_login = true
        // $scope.msg_login = 'Error, credenciales erroneas'
        $ionicPopup.alert({
           title: 'Hubo un error!',
           template: 'Credenciales erroneas'
         });
      } else {
        localStorage.setItem("sesion", "1");
        $scope.show_login = false
        $scope.modal.hide();
        location.reload()
      }
    })
  };

  $scope.btnCerrarSesion = function () {
    localStorage.removeItem("sesion");
    location.reload()
  }
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {

})

.controller('inicioCtrl', function($scope, $stateParams) {

})

.controller('agregarCtrl', function($scope, $stateParams, $ionicPopup, $ionicModal, $http, $ionicActionSheet, $timeout) {

  $ionicModal.fromTemplateUrl('my-modal.html', {
     scope: $scope,
     animation: 'slide-in-up'
   }).then(function(modal) {
     $scope.modal = modal;
   });

  $scope.openModalDesc = function(d) {
     console.log(d,'************');
     var arr = []
     arr.push(d)
     $scope.seleccion = arr
     $scope.modal.show();
  };

  $scope.hideModal = function () {
    $scope.modal.hide();
  }

  $scope.agregar = {}
  $scope.btnAgregar = btnAgregar

  // $scope.lista = [
  //     {nombre: 'Jose Osvaldo', apellido: 'Hernandez Rito', lenguaje: 'JavaScript', img:'img/js.png' ,  edad:22},
  //     {nombre: 'Armando',      apellido: 'Villegas',       lenguaje: 'C++',        img:'img/c.png'  ,  edad:24},
  //     {nombre: 'Yahir',        apellido: 'Oropeza',        lenguaje: 'SQL',        img:'img/sql.png',  edad:28}
  // ]

  $http.get('https://api-quinfort.herokuapp.com/get')
  .then(function(res){
    if (!res.data.err) {
      var d = res.data.res
      for (var key in d) {
        if (d[key].lenguaje_id == 1) {
          d[key].img = "img/js.png"
          d[key].lenguaje = "JavaScript"
        } else if (d[key].lenguaje_id == 2) {
          d[key].img = "img/c.png"
          d[key].lenguaje = "C++"
        } else if (d[key].lenguaje_id == 3) {
          d[key].img = "img/sql.png"
          d[key].lenguaje = "SQL"
        } else {
          d[key].img = "img/user.png"
        }
      }
      $scope.lista = d
    } else {
      $ionicPopup.alert({
         title: 'Hubo un error!',
         template: 'Al tratarse de conectar con el server'
       });
    }
  })

  $scope.lenguaje = [
    {id:1 ,lenguaje:'JavaScript'},
    {id:2 ,lenguaje:'C++'},
    {id:3 ,lenguaje:'SQL'},
    {id:4 ,lenguaje:'Python'},
    {id:5 ,lenguaje:'C'},
    {id:6 ,lenguaje:'Java'},
    {id:7 ,lenguaje:'Ruby'},
    {id:8 ,lenguaje:'Swift'},
    {id:9 ,lenguaje:'Objective-c'},
    {id:10 ,lenguaje:'C Sharp'},
    {id:11 ,lenguaje:'NoSql'},
    {id:12 ,lenguaje:'PHP'},
    {id:13 ,lenguaje:'Pascal'}
  ]

  function readyAgregar() {
    var d = $scope.agregar
    return d.nombre != null && d.apellido != null  && d.edad != null
  }

  function btnAgregar() {
    if (readyAgregar()) {
      var d = $scope.agregar
      $http.post('https://api-quinfort.herokuapp.com/set', d)
      .then(function(res){
        console.log(res);
        if (res.status != 200) {
          $ionicPopup.alert({
             title: 'Hubo un error!',
             template: 'No se puedo conectar al server'
           });
        } else {
          if (!res.data.err) {
            location.reload()
          } else {
            $ionicPopup.alert({
               title: 'Hubo un error!',
               template: 'No se puedo conectar a la base de datos'
             });
          }
        }
      })
    } else {
      $ionicPopup.alert({
         title: 'Hubo un error!',
         template: 'Todos los campos con * son requeridos'
       });
    }
  }

  $scope.onHold = function (d) {
    var hideSheet = $ionicActionSheet.show({
     destructiveText: 'Eliminar',
     destructiveButtonClicked: function () {
       $http.post('https://api-quinfort.herokuapp.com/del',d)
       .then(function(res){
         location.reload()
       })
     },
     titleText: 'Seleccionaste ' + d.nombre,
     cancelText: 'Cancel',
     cancel: function() {
          hideSheet();
     }
   })
  }

})

.controller('sobreCtrl', function($scope, $stateParams) {

})
