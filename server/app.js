var express = require('express')
var app = express()
var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser());

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.header('Access-Control-Allow-Credentials', true);
  return next();
});

app.get('/', inicio)
app.post('/do-login', doLogin)

function inicio(req, res) {
  res.send('Hola')
}

function doLogin(req, res) {
  var usr = 'admin'
  var pass = '1234'
  var d = req.body;

  if (d.username == usr && d.password == pass) {
    res.json({err:false})
  } else {
    res.json({err:true, description: 'Credenciales invalidadas'})
  }
}

app.listen(4001, function(){
  console.log('Escuchando en el puerto 4001');
})
